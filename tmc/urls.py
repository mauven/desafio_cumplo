from django.conf.urls import url
from . import views

namespace = 'tmc'
app_name = 'tmc'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url('calculate', views.calculate, name='calculate'),
    url('home', views.home, name='home'),
]
