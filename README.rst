==============
Desafío cumplo
==============

Introducción
------------

Este proyecto consiste en resolver la problemática de encontrar la tasa máxima convencional
(TCM) que aplica para un determinado monto de crédito, en un día cualquiera.

Se asumen las condiciones para el ejercicio de cálculo de TCM, de acuerdo a los datos que entrega la SBIF [4].


Ejecución
---------

1. Cargar el entorno virtual para python 3.8.

2. Ingresar en el directorio del proyecto.

3. Ejecutar la aplicación.

4. Ingresar en la siguiente url:

    http://127.0.0.1:8000/tmc/

Referencias
-----------

1. ¿En qué consiste la Tasa de Interés? [http://www.cmfchile.cl/educa/600/w3-article-27169.html]
2. Tasa de Interés Máxima Convencional [https://www.sernac.cl/portal/619/w3-article-58122.html]
3. Tasa de Interés Corriente y Máxima Convencional [https://sbif.cl/sbifweb/servlet/InfoFinanciera?indice=4.2.1&FECHA=7/2/2020]
4. API SBIF [https://api.sbif.cl/documentacion/TMC.html]

by mauven@gmail.com
