from django.shortcuts import render, get_object_or_404
import requests
from django.http import HttpResponse, HttpResponseRedirect

from datetime import datetime, date, time, timedelta
from django.template import loader, RequestContext
from decimal import Decimal


# Create your views here.
def index(request):
    context = {}
    return render(request, 'tmc/index.html', context)


def calculate(request):
    if request.method == 'POST':

        data = request.POST.copy()
        print(data)

        # Fecha cuando se contrató el crédito
        contract_date = data.get('contract_date')
        print("contract_date: " + contract_date)
        contract_date_dt = datetime.strptime(contract_date, '%Y-%m-%d').date()

        # Fecha de consulta de tmc
        tmc_query_date = data.get('tmc_query_date')
        tmc_query_date_dt = datetime.strptime(tmc_query_date, '%Y-%m-%d').date()

        # Monto en UF del crédito
        amount = int(data.get('amount'))

        # Período en dias
        credit_term = int(data.get('credit_term'))

        # Fecha que se acaba el plazo de pago
        deadline_payment = contract_date_dt + timedelta(days=credit_term)

        # Diferencia de días entre el plazo y la fecha de consulta tmc.
        # Si el valor es negativo, no aplica tmc.
        datetime_delta = tmc_query_date_dt - deadline_payment
        debt_days = datetime_delta.days

        # print("contract_date: " + contract_date)
        print("contract_date_dt: " + str(contract_date_dt))
        print("credit_term: " + str(credit_term))
        print("deadline_payment: " + str(deadline_payment))
        # print("datetime_delta: " + str(datetime_delta))
        print("debt_days: " + str(debt_days))

        template = loader.get_template('tmc/result.html')
        ctx = {}

        if debt_days <= 0:
            ctx = {'message': "No hay mora"}
            return HttpResponse(template.render(ctx))

        # Operaciones no reajustables en moneda nacional de menos de 90 días
        if credit_term < 90:

            # Inferiores o iguales al equivalente de 5.000 unidades de fomento
            # Tipo: 26
            # Ejemplo: 32,1 %
            # Prueba: 400 UF, 60 dias, 01/11, 15/01
            if amount <= 5000:

                print("Calculando Tipo 26")
                tmc_api_type = 26
                api_tmc_rate = ask_rate_to_api(tmc_query_date_dt, tmc_api_type)
                rate_result = adjust_period_rate(api_tmc_rate, debt_days) + ' %'

            # Superiores al equivalente de 5.000 unidades de fomento
            # Tipo: 25
            # Ejemplo: 6,81
            # Prueba: 8000 UF, 60 dias, 01/11, 15/01
            elif amount > 5000:

                print("Calculando Tipo 25")
                tmc_api_type = 25
                api_tmc_rate = ask_rate_to_api(tmc_query_date_dt, tmc_api_type)
                rate_result = adjust_period_rate(api_tmc_rate, debt_days) + ' %'

        # Operaciones no reajustables en moneda nacional 90 días o más
        elif credit_term >= 90:

            # Inferiores o iguales al equivalente de 50 unidades de fomento
            # Tipo: 45
            # Ejemplo: 33,96 %
            # Prueba: 25 UF, 100 dias, 01/10, 16/01
            if amount <= 50:

                print("Calculando Tipo 45")
                tmc_api_type = 45
                api_tmc_rate = ask_rate_to_api(tmc_query_date_dt, tmc_api_type)
                rate_result = adjust_period_rate(api_tmc_rate, debt_days) + ' %'

            # Inferiores o iguales al equivalente de 200 unidades de fomento  y superiores al equivalente de 50
            # unidades de fomento
            # Tipo: 44
            # Ejemplo: 26,96 %
            # Prueba: 100 UF, 100 dias, 01/10, 16/01
            elif (amount > 50) & (amount <= 200):

                print("Calculando Tipo 44")
                tmc_api_type = 44
                api_tmc_rate = ask_rate_to_api(tmc_query_date_dt, tmc_api_type)
                rate_result = adjust_period_rate(api_tmc_rate, debt_days) + ' %'

            # Inferiores o iguales al equivalente de 5.000 unidades de fomento y superiores al equivalente de 200
            # unidades de fomento
            # Tipo: 35
            # Ejemplo: 19,44
            # Prueba: 300 UF, 100 dias, 01/10, 16/01
            elif (amount > 200) & (amount <= 5000):

                print("Calculando Tipo 35")
                tmc_api_type = 35
                api_tmc_rate = ask_rate_to_api(tmc_query_date_dt, tmc_api_type)
                rate_result = adjust_period_rate(api_tmc_rate, debt_days) + ' %'

            # Superiores al equivalente de 5.000 unidades de fomento
            # Tipo: 34
            # Ejemplo: 5,63 %
            # Prueba: 8000 UF, 100 dias, 01/10, 16/01
            if amount > 5000:

                print("Calculando Tipo 34")
                tmc_api_type = 34
                api_tmc_rate = ask_rate_to_api(tmc_query_date_dt, tmc_api_type)
                rate_result = adjust_period_rate(api_tmc_rate, debt_days) + ' %'


        # Parámetro del plazo no válido (NOK)
        else:
            return HttpResponseRedirect('/tmc/')

        # Envía resultado a pantalla (OK)
        print("rate_result: " + str(rate_result))
        ctx = {'rate_result': rate_result}
        return HttpResponse(template.render(ctx))

    # Método del request distinto de POST (NOK)
    else:
        return HttpResponseRedirect('/tmc/')


def tem(_tea):
    return (1 + _tea) ** (1 / 12) - 1


def ted(_tem):
    return (1 + _tem) ** (1 / 30) - 1


def period_rate(_ted, t):
    return (1 + _ted) ** t - 1


def home(request):
    return HttpResponse("HOME")


def adjust_period_rate(_api_tmc_rate, _debt_days):
    l_tem = tem(_api_tmc_rate / 100)
    l_ted = ted(l_tem)
    l_period_rate = period_rate(l_ted, _debt_days) * 100

    rate_result = str('{:.3f}'.format(l_period_rate))
    return rate_result


def ask_rate_to_api(_tcm_query_date_dt, _tmc_api_type):
    print("_tcm_query_date_dt: " + str(_tcm_query_date_dt))
    print("_tmc_api_type:" + str(_tmc_api_type))
    test_date_dt = _tcm_query_date_dt

    # Se considera un periodo de consulta para obtener los datos de la API
    prev_test_date_dt_year = str((test_date_dt - timedelta(days=90)).year)
    prev_test_date_dt_month = str((test_date_dt - timedelta(days=90)).month)
    next_test_date_dt_year = str((test_date_dt + timedelta(days=90)).year)
    next_test_date_dt_month = str((test_date_dt + timedelta(days=90)).month)

    api_key = '9c84db4d447c80c74961a72245371245cb7ac15f'
    query_format = 'json'

    url_request = 'https://api.sbif.cl/api-sbifv3/recursos_api/tmc/periodo/'
    url_request += prev_test_date_dt_year + '/' + prev_test_date_dt_month + '/' + next_test_date_dt_year + '/' + next_test_date_dt_month
    url_request += '?apikey=' + api_key + '&formato=' + query_format

    response_api = requests.get(url_request)
    # print(response_api.status_code)

    rate_api_value = 0

    if response_api.status_code == 200:

        data = response_api.json()

        # Elemento TMC de ejemplo que responde la API
        #
        #  {
        #      'Titulo': 'Operaciones no reajustables en moneda nacional de menos de 90 días',
        #      'SubTitulo': 'Superiores al equivalente de 5.000 unidades de fomento',
        #      'Valor': '6.63',
        #      'Fecha': '2019-01-15',
        #      'Hasta': '2019-02-14',
        #      'Tipo': '25'
        #  },

        # Se recorren todos los elementos TMC para el período establecido
        for single_data in data['TMCs']:

            tmc_from_date = single_data['Fecha']
            tmc_from_date_dt = datetime.strptime(tmc_from_date, '%Y-%m-%d').date()

            # La fecha de fin del período de tmc existe
            if single_data.get('Hasta') is not None:
                tmc_to_date = single_data['Hasta']
                tmc_to_date_dt = datetime.strptime(tmc_to_date, '%Y-%m-%d').date()

                if (tmc_from_date_dt <= test_date_dt) & (test_date_dt <= tmc_to_date_dt):
                    # print(tmc_from_date_dt)
                    # print(tmc_to_date_dt)

                    if int(single_data['Tipo']) == int(_tmc_api_type):
                        print("valor: " + str(single_data['Valor']))
                        rate_api_value = float(single_data['Valor'])

            # La fecha de fin del período no existe (tasa vigente)
            else:
                if tmc_from_date_dt <= test_date_dt:
                    # print(tmc_from_date_dt)

                    if int(single_data['Tipo']) == int(_tmc_api_type):
                        rate_api_value = float(single_data['Valor'])

        return rate_api_value

    else:
        return HttpResponse(str(response_api.status_code))
